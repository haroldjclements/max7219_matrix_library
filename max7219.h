/*
 * Program: max7219.h
 * Author: Andrey Ovcharov (initial creator) & Harold Clements
 *
 * Description:
 * This helper library allows for simple use of the MAX7219 device.
 * The MAX7219 is a Serially Interfaced, 8-Digit LED Display Driver.
 *
 * Limitations:
 * Currently only supports a single 8x8 LED Matrix. 
 * The ASCII to bitmap lookup does not contain the full 255 characters. 
 *
 * External References:
 * https://github.com/snakeye/avr-projects/tree/master/max7219
 * https://www.maximintegrated.com/en/products/MAX7219
 * http://www.phanderson.com/bx24/max7219.html
 * 
 * Change History:
 * Version 1.0.0.0 [20031115] Original Version
 * Version 1.1.0 0 [20160605] Variable and Comment refactoring.
 * Version 1.2.0.0 [20160616] Added string processing with time delay and debug utility.
 * Version 1.3.0.0 [20160625] Data array bitmap is now created and passed, not returned.
 *                            Added a clear column function, added a more fine grained debugging function.
 * Version 1.4.0.0 [20160627] Added a function to scroll text from left to right. 
 */ 

#ifndef MAX7219_H_
#define MAX7219_H_

/*
 * Register Address Map
 */
#define MAX7219_RAM_DECODE 		0x09	//0b00001001
#define MAX7219_RAM_INTENSITY 	0x0A	//0b00001010 [Brightness]
#define MAX7219_RAM_SCAN 		0x0B	//0b00001011
#define MAX7219_RAM_SHUTDOWN 	0x0C	//0b00001100
#define MAX7219_RAM_TEST 		0x0F	//0b00001111

#include <stdbool.h>			// Contains definitions for bool

/**
 * Initialisation for setting up the MAX7219 device.  
 *
 * return: void 
 */
void max7219_init( );

/**
 * Writes the data to a specific register.
 * The two input parameter bytes are shifted into the internal 16-bit register. 
 * The load pin is turned on before the data sent to the max7219_write function.
 * After both the data address and digit data are sent to the MAX7219, 
 *
 * | D15  D14  D13  D12   D11  D10  D09  D08 |  D07  D06  D05  D04  D03  D02  D01  D00 |
 * |  X    X    X    X  |       ADDRESS      |  MSB              DATA              LSB |
 * 
 * param: uint8_t addr - the address register to which to write the data.
 * param: uint8_t data - a byte string of data to be written to the register.
 * return: void 
 */
void max7219_write( uint8_t addr, uint8_t data );

/**
 * Iterates through the byte within the data array, sending each byte in turn to 
 * the max7219_write function.   
 *
 * param: const uint8_t* data - a pointer to the 'data' byte (character bitmap) array.
 * return: void 
 */
void max7219_writebytes( const uint8_t* data );

/**
 * Iterates through a given string each character at a time. A new byte array (pointer) is created with 8 elements.
 * This pointer is then passed to the max7219_charmappings function to populate the data pointer with the 
 * relevant bitmap array. The populated pointer is then passed to the max7219_writebytes function for display. 
 * The process then pauses for a given amount of time (delayms) before deleting the data array bitmap and 
 * continuing on with the next letter in the given string ( string ).
 *
 * param: const char* character - a pointer to the string array for the message to be displayed.
 * param: int delayms - the time (in milliseconds) to delay before processing the next character.
 * return: void 
 */
void max7219_writestring( const char* string, int delayms );

/**
 * Iterates through a given string each character at a time. A new byte array (pointer) is created with 8 elements.
 * This pointer is then passed to the max7219_charmappings function to populate the data pointer with the 
 * relevant bitmap array. A temporary byte array is created and reverse bit-shifted with the populated data array.
 * This scrolls the character in from the left side of the display, moving right. The populated data array is 
 * bit-shifted one to the right to simulate the character scrolling out from the right.
 *
 * param: const char* character - a pointer to the string array for the message to be displayed.
 * param: int delayms - the scroll speed (in milliseconds) and the before the next character.
 * return: void 
 */
void max7219_scrollstring( const char* string, int delayms );

/**
 * The lookup table (switch) matches the ASCII decimal value (character) and copies the relevant bitmap array into 
 * the (data) given pointer. memcpy_P is used as the font collection (bitmap byte arrays) are stored in the 
 * microcontroller's flash (program space).
 *
 * param: const char character - the lookup character for the relevant bitmap array.
 * param: uint8_t *data - a pointer to the 8 bite 'data' array.
 * return: void 
 */
 void max7219_charmappings( const char character, uint8_t* data );

/**
 * Will iterate over every bit within the 'data' byte, turning off the CLK pin and testing to see if the most significant bit ( & 0x80 ) is set.
 * If true, the DIN pin is set to on. If false, the DIN pin is set to off.
 * The data byte is then bit shift one to the left ( << 1 ), the CLK pin turned back on and the iteration is repeated.
 *
 * param: data - a byte string representing the LEDs to be illuminated.
 * return: void 
 */
void max7219_send( uint8_t data );

/**
 * Clears (extinguishes) all the LEDs in a given (row).
 *
 * param: uint8_t row - the row to be cleared.
 * return: void
 */
void max7219_clearrow( uint8_t row );

/**
 * Clears (extinguishes) all the LEDs.
 *
 * return: void
 */
void max7219_clear( );

 /**
  * Toggles the RAM_TEST mode.
  * 
  * param: is_enable - boolean for toggling the RAM_TEST mode. 
  *        True for enabling RAM_TEST, False for disabling. 
  * return: void
  */
void max7219_test( bool on );
 
 /**
  * Toggle for enabling RAM_SHUTDOWN.
  *
  * param: enable_shutdown - boolean for toggling the RAM_SHUTDOWN mode. 
  *        True for enabling RAM_SHUTDOWN, False for disabling (normal operation). 
  * return: void
  */
void max7219_shutdown( bool shutdown );
 
 /**
  * A debugging utility that displays unassigned 8bit integers (uint8_t) for three seconds
  * on the first column for the 8x8 matrix, after which it clears the display.  
  *
  * param: uint8_t data, - the debug data to be written first column.
  * return: void
  */
void debug( uint8_t data );

 /**
  * A debugging utility that displays unassigned 8bit integers (uint8_t) for three seconds
  * on the first column for the 8x8 matrix, after which it clears the display.  
  *
  * param: uint8_t data, - the debug data to be written first column.
  * param: uint8_t column - the column to be cleared.
  * param: int delayms - the delay (in milliseconds) to pause before continuing.
  * return: void
  */
void debugc( uint8_t data, uint8_t column, int delayms  );
 

#endif /* MAX7219_H_ */