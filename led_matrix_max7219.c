/*
 * Program: led_maxrix_max7219.c
 * Author: Harold Clements
 *
 * Description:
 * This example will illuminate LEDs within the 8x8 matrix. 
 *
 * Connections:
 * PB3 [Output]		T85 [pin2] -> LED_Matrix.CLK
 * PB4 [Output]		T85 [pin3] -> LED_Matrix.LOAD
 * PB1 [Output]		T85 [pin6] -> LED_Matrix.DIN 
 *
 * Overview:
 * The LED Matrix will illuminate one character at a time to display:
 *
 * LED Matrix Test. . .
 * How much wood would a woodchuck chuck?
 *  
 * Hardware:
 * 1 x Atmel ATtiny85 Microprocessor.
 * 1 x MAX7219 8x8 LED Matrix.
 * 
 * Libraries (include):
 * max7219 - Version 1.4.x.x [20160627] - A MAX7219 Helper Library by Andrey Ovcharov (snakeye) & Harold Clements
 *
 * External References:
 * https://github.com/snakeye/avr-projects/tree/master/max7219
 * 
 * Change History:
 * Version 0.1.0.0 [20160528] Pre-Release Version.
 * Version 1.0.0.0 [20160529] Original Version.
 * Version 1.0.1.0 [20160616] Updated Comments and Display String.
 * Version 1.1.0.0 [20160627] Added scrolling text and corrected typo in comments.
 */
 
#ifndef F_CPU	// Code should be executed if F_CPU is not defined in Makefile.
#warning "F_CPU not defined in makefile - using definition in led_matrix_max7219.c"
#define F_CPU 1000000UL	// Set to 1MHz CPU cycles per second.
#endif

// The #define F_CPU needs to be set before this #include.
#include <avr/io.h> 						// Contains definitions for DDRB, PORTB
#include <util/delay.h>						// Contains definitions for _delay_ms.  
#include "max7219.h"		// Contains definitions for max7219_init, max7219_write

int main( void ) {
	
	max7219_init();				//initialise the led matrix.

	/*
	 * Test Card - Draw a square of squares.
	 */
	max7219_write(1, 255);
	max7219_write(2, 153);
	max7219_write(3, 153);
	max7219_write(4, 255);
	max7219_write(5, 255);
	max7219_write(6, 153);
	max7219_write(7, 153);
	max7219_write(8, 255);
	
	_delay_ms( 2000 );		//delay of 2000ms (2 Sec).
	max7219_clear( );		//clear all the LEDs.
	
	for( ;; ) {				//loops forever; same as while( 1 );
		//write a string of characters to device with the delay (in milliseconds) between each character being displayed.
		max7219_writestring( "LED Matrix Test. . .", 700 );							//display one character after another.
		max7219_scrollstring( "How much wood would a woodchuck chuck?", 100 );		//scroll characters left to right. 
	}
  	return 0;	//for completeness (this code is never reaches). 
}
