#------------------------------------------------------------------
# Makefile Description
# Basic Makefile for AVR hex compiling and device flashing.
#
# Flash Programmer Support
# usbASP, avrisp (arduino)
#
# Device Support
# attiny85, atmega168, atmega328
#
# Author: Harold Clements
# Version: 1.0.0 [20160527] - Original SVN Checked Version.  
# Version: 1.0.1 [20160530] - Added description. 
#-------------------------------------------------------------------

#---------------------------
# Program Description
# Builds and flashes the .hex file as per led_matrix_max7219.c 
#
# Libraries (include)
# max7219 - Version 1.x.x [20031115] - A MAX7219 Helper Library by Andrey Ovcharov (snakeye) 
#--------------------------

#--------------------------
# Micro-controller Settings
#--------------------------

MCU		 	 =	attiny85
EFUSE		 =	0xFF
HFUSE		 =	0xDF
LFUSE		 =	0x62

# MCU		 	 =  atmega168	
# EFUSE		 =	0xF9
# HFUSE		 =	0xDF
# LFUSE		 =	0x62

#MCU		 =  	m328p	
#EFUSE		 =	0xFF
#HFUSE		 =	0xD9
#LFUSE		 =	0x62	

MCU_BAUDRATE =	38400

#--------------------------
# Programmer Settings
#--------------------------
#
# usbasp
PORT		 =	USB
PROGRAMMER	 =	usbasp 

# arduino (alt. programmer stk500v1)
#PROGRAMMER	 =	arduino
#PROGRAMMER	 =	avrisp
#PORT		 =	COM3

PRG_BAUDRATE 	 =	19200
PRG_BITCLOCK 	 =	4
AVR_SOFTWARE 	 =	avrdude
DEBUG		 =	dwarf-2
OBJFLAGS	 =	-j .text -j .data -j .eeprom -j .fuse -O ihex

#Programmer Flags
# -p		 =	Micro-controller Type
# -P		 = 	COMM PORT
# -b		 =  	Programming Baud Rate (if using RS232) [Required for ArduinoISP]
# -c		 =  	Programmer type
# -B		 =  	Programmer Write Speed
# -n		 =	Write-nothing mode (for testing)
# -F		 = 	Force!
FFLAGS		 =  -p $(MCU)
FFLAGS		 += -P $(PORT) 
FFLAGS		 += -b $(PRG_BAUDRATE)
FFLAGS		 += -c $(PROGRAMMER)
FFLAGS		 += -B $(PRG_BITCLOCK)
#FFLAGS		 += -n
#FFLAGS		 += -F
#--------------------------

#--------------------------
# Tokens
#--------------------------
F_CPU		= 	1000000
MCU_BAUDRATE	= 	38400
MAX7219_PORT	=	PORTB
#--------------------------

#--------------------------
#Compiler Settings
#--------------------------
CC			= 	avr-gcc
TARGET			=	led_matrix_max7219

INCLUDE_SRC_DIR	 	=	
INCLUDE_SRC_FILE 	=	max7219.c
OBJCOPY		 	=	avr-objcopy
LIBS		 	=	
#Build Settings - Creates a list of source files to be processed.
BUILD_SRC		=	$(TARGET).c
_BUILD_SRC		=   $(addprefix $(INCLUDE_SRC_DIR), $(INCLUDE_SRC_FILE))
BUILD_SRC		+=	$(_BUILD_SRC)

#
#Compiler Options
# -mmcu			=	Set the micro-controller to compile to.
# -DF_CPU		=	Sets the micro-controller clock cycles. 
# -DBAUD		=	Baud Speed of the MCU
# -I<dir>		=	Add the directory <dir> to the head of the list of directories to be searched for header files. 
# -Wall			=	All warnings.
# -Wunused-variable 	= 	Suppress unused variable warnings
# -Os			=	Performs further optimizations designed to reduce code size.
# -o			=	Write the build output to an output file.
# -g*			=	Generate debugging information.
CFLAGS			=	-mmcu=$(MCU)
CFLAGS	   		+=	-DF_CPU=$(F_CPU)
#CFLAGS	   		+=	-DBAUD=$(MCU_BAUDRATE)
CFLAGS			+=	-I.
CFLAGS			+=	-I$(INCLUDE_SRC_DIR)
CFLAGS	   		+=	-Wall
CFLAGS	   		+=	-Os
CFLAGS	   		+=	-o 
#CFLAGS 		+=	-g$(DEBUG)
#--------------------------

#--------------------------
#Build the code
#--------------------------
all: begin $(TARGET).hex end

begin :
	@echo --- Starting ---
	@echo
	
end :
	@echo
	@echo --- Ended ---

%.hex: %.elf
	$(OBJCOPY) $(OBJFLAGS) $< $@

%.elf: $(BUILD_SRC)
	$(CC) $(CFLAGS) $@ $(BUILD_SRC)  

#--------------------------
#Clean Up Options
#--------------------------	
clean:
	rm -rf *.elf *.hex

#--------------------------
#Flash Options
#--------------------------
flash: $(TARGET).hex
	$(AVR_SOFTWARE) $(FFLAGS) -e -U flash:w:$<
	
status:
	$(AVR_SOFTWARE) $(FFLAGS) -v

backup: $(TARGET).hex
	$(AVR_SOFTWARE) $(FFLAGS) -U flash:r:$<.bin:r

fuse_info:
	$(AVR_SOFTWARE) $(FFLAGS) -u -q -U hfuse:r:-:i -U efuse:r:-:i -U lock:r:-:i 
	
fuse_blow:
	$(AVR_SOFTWARE) $(FFLAGS) -U efuse:w:$(EFUSE):m  
	$(AVR_SOFTWARE) $(FFLAGS) -U hfuse:w:$(HFUSE):m 
	$(AVR_SOFTWARE) $(FFLAGS) -U lfuse:w:$(LFUSE):m 
	
erase:
	$(AVR_SOFTWARE) $(FFLAGS) -e 
		
#--------------------------
#Information
#--------------------------
	
#GET THE PACKAGES (Linux)
#sudo pacman -S gcc-avr avr-libc avrdude
#sudo apt-get install avrdude avr-libc binutils-avr gcc-avr
#
#GET THE PACKAGES (Windows)
# WinAVR-20100110-install
# avrdude-6.1-mingw32

#BUILD THE CODE
#avr-gcc -mmcu=attiny85 -Wall -Os -o blink.elf blink.c
#avr-objcopy -j .text -j .data -O ihex blink.elf blink.hex

#FLASH TO CHIP
#avrdude -p t85 -c usbasp -e -U flash:w:blink.hex

#FUSE INFO (http://www.engbedded.com/fusecalc)
#avrdude -p t85 -u -P USB -b 38400 -c usbasp -q -U lfuse:r:-:i -U hfuse:r:-:i -U efuse:r:-:i -U lock:r:-:i 
#BLOWING FUSES
#avrdude -p t85 -P USB -b 38400 -c usbasp -U lfuse:w:0x62:m -v
#avrdude -p t85 -P USB -b 38400 -c usbasp -U hfuse:w:0xDF:m -v
#avrdude -p t85 -P USB -b 38400 -c usbasp -U efuse:w:0xFF:m -v

#FULL LIST OF PROGRAMMERS SUPPORTED BY AVRDUDE
#avrdude -c ?

# $@ = blink.hex
# $< = blink.elf
