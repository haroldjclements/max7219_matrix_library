/*
 * Program: max7219.c
 * Author: Andrey Ovcharov (initial creator) & Harold Clements
 *
 * Description:
 * This helper library allows for simple use of the MAX7219 device.
 * The MAX7219 is a Serially Interfaced, 8-Digit LED Display Driver.
 *
 * Limitations:
 * Currently only supports a single 8x8 LED Matrix. 
 * The ASCII to bitmap lookup does not contain the full 255 characters. 
 *
 * External References:
 * https://github.com/snakeye/avr-projects/tree/master/max7219
 * https://www.maximintegrated.com/en/products/MAX7219
 * http://www.phanderson.com/bx24/max7219.html
 * 
 * Change History:
 * Version 1.0.0.0 [20031115] Original Version
 * Version 1.1.0.0 [20160529] Port change for Ateml ATtiny85.
 * Version 1.1.1.0 [20160605] Variable and Comment refactoring.
 * Version 1.2.0.0 [20160616] Added string processing with time delay and debug utility.
 * Version 1.3.0.0 [20160625] Data array bitmap is now created and passed, not returned.
 *                            Added a clear column function, added a more fine grained debugging function. 
 * Version 1.4.0.0 [20160627] Added a function to scroll text from left to right.
 */

/*
 * The MAX7219 includes 16 registers.
 *
 * Registers 1 - 8 are used to store the data output for each displays (1 - 8).
 * Register 9 is used for defining Code B (BCD) Translations.
 * Registers 10 & 11 are used to be define the Intensity (Brightness).
 * Register 12 controls the display (on or shutdown).
 *
 */
 
#include <avr/io.h>			// Contains definitions for DDRB, PORTB
#include <util/delay.h>		// Contains definitions for _delay_ms. 
#include <string.h>			// Contains definitions for memcpy
#include <stdlib.h>			// Contains definitions for malloc.

#include "max7219.h"
#include "vga_rom.h"

#define MAX7219_PORT 	PORTB
#define DIN 			PB1		//0b00010
#define CLK 			PB3		//0b01000
#define LOAD 			PB4		//0b10000

/**
 * Will iterate over every bit within the 'data' byte, turning off the CLK pin and testing to see if the most significant bit ( & 0x80 ) is set.
 * If true, the DIN pin is set to on. If false, the DIN pin is set to off.
 * The data byte is then bit shift one to the left ( << 1 ), the CLK pin turned back on and the iteration is repeated.
 *
 * param: uint8_t data - a byte string representing the LEDs to be illuminated.
 * return: void 
 */
void max7219_send( uint8_t data ) {
	static uint8_t i;							//loop control.
	asm( "nop" );								//executes "nothing" for one machine cycle.

	for( i = 0; i < 8; i++ )  {					//iterate over every bit within the byte.
		MAX7219_PORT &= ~( 1 << CLK );			//turn the CLK pin off.
		asm( "nop" );							//executes "nothing" for one machine cycle.
		if( data & 0x80 )						//will return true if the eight (most significant bit) is set.
			MAX7219_PORT |= ( 1 << DIN );		//turn the DIN pin on (if not already).
		else 
			MAX7219_PORT &= ~( 1 << DIN );		//turn the DIN pin off.
		data = data << 1;						//bit shift one to the left.  
		MAX7219_PORT |= ( 1 << CLK );			//turn the CLK pin on (if not already).
	}
}

/**
 * Writes the data to a specific register.
 * The two input parameter bytes are shifted into the internal 16-bit register. 
 * The load pin is turned on before the data sent to the max7219_write function.
 * After both the data address and digit data are sent to the MAX7219, 
 *
 * | D15  D14  D13  D12   D11  D10  D09  D08 |  D07  D06  D05  D04  D03  D02  D01  D00 |
 * |  X    X    X    X  |       ADDRESS      |  MSB              DATA              LSB |
 * 
 * param: uint8_t addr - the address register to which to write the data.
 * param: uint8_t data - a byte string of data to be written to the register.
 * return: void 
 */
void max7219_write( uint8_t addr, uint8_t data ) {
	MAX7219_PORT |= ( 1 << LOAD );				//turn the LOAD pin on (if not already).
	max7219_send( addr );						//send the address byte as first 8 bits.
	max7219_send( data );						//closely followed by 2nd 8 bits (the digit data byte).
	MAX7219_PORT &= ~( 1 << LOAD );				//turn the LOAD pin off.
	asm( "nop" );								//executes "nothing" for one machine cycle.
	MAX7219_PORT |= ( 1 << LOAD );				//turn the LOAD pin on (if not already).
}

/**
 * Iterates through the byte within the data array, sending each byte in turn to 
 * the max7219_write function.   
 *
 * param: const uint8_t* data - a pointer to the 'data' byte (character bitmap) array.
 * return: void 
 */
void max7219_writebytes( const uint8_t* data ) {
	
	uint8_t i;									//loop control initialisation.
	for( i = 0; i < 8; i++) {					//iterate over every bit within the byte.
		/*
		 * The character bitmaps are stored in the microcontroller's flash memory.
		 * The pgm_read_byte allows access to flash memory address byte by byte.
		 * Each byte within the array is passed the max7219_write function along with
		 * its base1 index within the array.
		 *
		 * Note: In the previous version, the pointer passed to this function was contained 
		 *       in the flash memory (program space) partition. This required the 
		 *       pgm_read_byte function to retrieve the correct bytes.
		 *
		 *     max7219_write( i+1, pgm_read_byte( data + i ) );
		 */
		max7219_write( i + 1, data[ i ] );		//write each byte in turn. 
	}
}

/**
 * Iterates through a given string each character at a time. A new byte array (pointer) is created with 8 elements.
 * This pointer is then passed to the max7219_charmappings function to populate the data pointer with the 
 * relevant bitmap array. The populated pointer is then passed to the max7219_writebytes function for display. 
 * The process then pauses for a given amount of time (delayms) before deleting the data array bitmap and 
 * continuing on with the next letter in the given string ( string ).
 *
 * param: const char* character - a pointer to the string array for the message to be displayed.
 * param: int delayms - the time (in milliseconds) to delay before processing the next character.
 * return: void 
 */
void max7219_writestring( const char* string, int delayms ) {
		
	uint8_t i = 0;									//loop control initialisation.
	while( string[ i ] != 0 ) {						//iterate over string until null terminator. 
		uint8_t* data = malloc( 8 );				//allocate 8 bites for the bit string.
		max7219_charmappings( string[ i ], data );	//pass data pointer to mapping function for population. 
		max7219_writebytes( data );					//pass the pointer to the data bitmap.
		_delay_ms( delayms );						//delay for the specified duration (delayms). 
		free( data );								//delete old data bitmap.
		i++;										//iterate to the next character within the string array.
	}
}

/**
 * Iterates through a given string each character at a time. A new byte array (pointer) is created with 8 elements.
 * This pointer is then passed to the max7219_charmappings function to populate the data pointer with the 
 * relevant bitmap array. A temporary byte array is created and reverse bit-shifted with the populated data array.
 * This scrolls the character in from the left side of the display, moving right. The populated data array is 
 * bit-shifted one to the right to simulate the character scrolling out from the right.
 *
 * param: const char* character - a pointer to the string array for the message to be displayed.
 * param: int delayms - the scroll speed (in milliseconds) and the before the next character.
 * return: void 
 */
void max7219_scrollstring( const char* string, int delayms ) {
	uint8_t i = 0;					//loop control.
	while( string[ i ] != 0 ) {		//iterate over string until null terminator 
		uint8_t* data = malloc( 8 );				//allocate 8 bites for the bit string.
		max7219_charmappings( string[ i ], data );	//pass data pointer to mapping function for population. 
		
		//_delay_ms( 700 );							//delay for the specified duration (delayms). 	
		
		if( string[ i ] != 0x20 ) {					//if not a whitespace.		

			uint8_t* shift_data = malloc( 8 );		//create a temporary byte array to hold the "run from right".
			uint8_t j, k;							//loop control initialisation for each bit and each byte (row).
			/*
			 * Run character from the left to the middle of the display by bit-shifting the data array backwards.    
			 */
			for( k = 0;  k < 8;  ++k) {				//iterate all eight bytes (rows) that makeup the bitmap.
				for (j = 0;  j < 8 ;  ++j) {		//iterate all eight bits that makeup the row.
					/*
					 * Copy the shifted contents of the data bitmap array into the temporary "shift_data".
					 */
					shift_data[ j ] = ( data[ j ] << ( 7 - k ) );
				}
				max7219_writebytes( shift_data );	//displaying the newly shifted bitmap.		
				_delay_ms( delayms );
			}
			free( shift_data ); 					//delete the temporary data array.
			
			/*
			 * Display the character in the center of the display.
			 */
			//max7219_writebytes( data );				//pass the pointer to the data bitmap.
			//_delay_ms( delayms );
			
			/*
			 * Run character from the middle out to the right of the display by bit-shifting one to the right. 
			 */
			for( k = 0;  k < 8; ++k) {				//iterate all eight bytes (rows) that makeup the bitmap.
				for (j = 0;  j < 8; ++j) {			//iterate all eight bits that makeup the row.
					data[ j ] = ( data[ j ] >> 1 ); //shifting one to the right. 
				}
				max7219_writebytes( data );			//displaying the newly shifted bitmap.
				_delay_ms( delayms );
			}
		}
		
		free( data );								//delete old data bitmap.
		i++;						//iterate to the next character within the string array.
	}
}

/**
 * The lookup table (switch) matches the ASCII decimal value (character) and copies the relevant bitmap array into 
 * the (data) given pointer. memcpy_P is used as the font collection (bitmap byte arrays) are stored in the 
 * microcontroller's flash (program space).
 *
 * param: const char character - the lookup character for the relevant bitmap array.
 * param: uint8_t *data - a pointer to the 8 bite 'data' array.
 * return: void 
 */
void max7219_charmappings( const char character, uint8_t* data ) {

	//debug( character, 1 );

	/*
	 * This switch statement acts as a lookup table that maps the ASCII decimal value with the bitmap byte array. 
	 * The corresponding bitmap array (within the flash memory) is copied to the (data) pointer using the memcpy_P
	 * function.
	 */
	switch( character ) {			//lookup table
		case  32: memcpy_P( data, SPACE, 8 );			break;	//  
		case  39: memcpy_P( data, APOSTROPHE, 8 );		break;	//' 
		case  46: memcpy_P( data, FULL_STOP, 8 );		break;	//.
		case  63: memcpy_P( data, QUESTION_MARK, 8);	break;	//?
		case  64: memcpy_P( data, AT, 8 );				break;	//@
		case  65: memcpy_P( data, CHAR_A, 8);			break;	//A
		case  66: memcpy_P( data, CHAR_B, 8);			break;	//B
		case  67: memcpy_P( data, CHAR_C, 8);			break;	//C
		case  68: memcpy_P( data, CHAR_D, 8);			break;	//D
		case  69: memcpy_P( data, CHAR_E, 8);			break;	//E
		case  70: memcpy_P( data, CHAR_F, 8);			break;	//F
		case  71: memcpy_P( data, CHAR_G, 8);			break;	//G
		case  72: memcpy_P( data, CHAR_H, 8 ); 			break;	//H
		case  73: memcpy_P( data, CHAR_I, 8 );			break;	//I
		case  74: memcpy_P( data, CHAR_J, 8 );			break;	//J
		case  75: memcpy_P( data, CHAR_K, 8 );			break;	//K
		case  76: memcpy_P( data, CHAR_L, 8 );			break;	//L
		case  77: memcpy_P( data, CHAR_M, 8 );			break;	//M
		case  78: memcpy_P( data, CHAR_N, 8 );			break;	//N
		case  79: memcpy_P( data, CHAR_O, 8 );			break;	//O
		case  80: memcpy_P( data, CHAR_P, 8 );			break;	//P
		case  81: memcpy_P( data, CHAR_Q, 8 );			break;	//Q
		case  82: memcpy_P( data, CHAR_R, 8 );			break;	//R
		case  83: memcpy_P( data, CHAR_S, 8 );			break;	//S
		case  84: memcpy_P( data, CHAR_T, 8 );			break;	//T
		case  85: memcpy_P( data, CHAR_U, 8 );			break;	//U
		case  86: memcpy_P( data, CHAR_V, 8 );			break;	//V
		case  87: memcpy_P( data, CHAR_W, 8 );			break;	//W
		case  88: memcpy_P( data, CHAR_X, 8 );			break;	//X
		case  89: memcpy_P( data, CHAR_Y, 8 );			break;	//Y
		case  90: memcpy_P( data, CHAR_Z, 8 );			break;	//Z
		case  95: memcpy_P( data, UNDERSCORE, 8 );		break;	//_
		case  97: memcpy_P( data, CHAR_a, 8 );			break;	//a
		case  98: memcpy_P( data, CHAR_b, 8 );			break;	//b
		case  99: memcpy_P( data, CHAR_c, 8 );			break;	//c
		case 100: memcpy_P( data, CHAR_d, 8 );			break;	//d
		case 101: memcpy_P( data, CHAR_e, 8 );			break;	//e
		case 102: memcpy_P( data, CHAR_r, 8 );			break;	//f
		case 103: memcpy_P( data, CHAR_g, 8 );			break;	//g
		case 104: memcpy_P( data, CHAR_h, 8 );			break;	//h
		case 105: memcpy_P( data, CHAR_i, 8 );			break;	//i
		case 106: memcpy_P( data, CHAR_j, 8 );			break;	//j
		case 107: memcpy_P( data, CHAR_k, 8 );			break;	//k
		case 108: memcpy_P( data, CHAR_l, 8 );			break;	//l
		case 109: memcpy_P( data, CHAR_m, 8 );			break;	//m
		case 110: memcpy_P( data, CHAR_n, 8 );			break;	//n
		case 111: memcpy_P( data, CHAR_o, 8 );			break;	//o
		case 112: memcpy_P( data, CHAR_p, 8 );			break;	//p
		case 113: memcpy_P( data, CHAR_q, 8 );			break;	//q
		case 114: memcpy_P( data, CHAR_r, 8 );			break;	//r
		case 115: memcpy_P( data, CHAR_s, 8 );			break;	//s
		case 116: memcpy_P( data, CHAR_t, 8 );			break;	//t
		case 117: memcpy_P( data, CHAR_u, 8 );			break;	//u
		case 118: memcpy_P( data, CHAR_v, 8 );			break;	//v
		case 119: memcpy_P( data, CHAR_w, 8 );			break;	//w
		case 120: memcpy_P( data, CHAR_x, 8 );			break;	//x
		case 121: memcpy_P( data, CHAR_y, 8 );			break;	//y
		case 122: memcpy_P( data, CHAR_z, 8 );			break;	//z
	}
}

/**
 * Initialisation for setting up the MAX7219 device.  
 *
 * return: void 
 */
void max7219_init() {
	/*
	 * Data Direction Register
	 */
	DDRB |= ( 1 << DIN ) | ( 1 << CLK ) | ( 1 << LOAD );	//set DIN, CLK and LOAD to 'Output'.	

	/*
	 * Register Address and the data values to be set next to the address. 
	 *
	 * Decode-Mode Register: No decoding; one-two-one mapping of the pins to LEDs.
	 * Scan Limit Register: Display digits (or rows in a matrix) 0 1 2 3 4 5 6 7.
	 * Intensity Register (Brightness): max on 31/32 duty cycle.
	 * Display-Test Register: Operates in two modes, normal and display test (turns all LEDs on).
	 * Shutdown Register:  
	 * 
	 */
	max7219_write( MAX7219_RAM_DECODE, 		0x00 );			//no decoder for digits 7-0
	max7219_write( MAX7219_RAM_SCAN, 		0x07 );			//all eight LED segments (rows) enable.
	max7219_write( MAX7219_RAM_INTENSITY,	0x0f );			//max brightness on 31/32 duty cycle.
	max7219_test( false );									//normal operation.			
	max7219_shutdown( false );								//normal operation.
}

/**
 * Clears (extinguishes) all the LEDs in a given (row).
 *
 * param: uint8_t row - the row to be cleared.
 * return: void
 */
 void max7219_clearrow( uint8_t row ) {
	max7219_write( row, 0x0 );			//write a binary zero to each of the rows. 
 }
 
 /**
 * Clears (extinguishes) all the LEDs.
 *
 * return: void
 */
 void max7219_clear( ) {
	uint8_t rows = 8;	
	while (rows) {						//iterate through each of the row.
		max7219_write( rows--, 0x0 );	//write a binary zero to each of the rows. 
	}
 }
 
 /**
  * Toggles the RAM_TEST mode.
  * 
  * param: is_enable - boolean for toggling the RAM_TEST mode. 
           True for enabling RAM_TEST, False for disabling. 
  * return: void
  */
  void max7219_test( bool is_enabled ) {
	if( is_enabled ) 
		max7219_write( MAX7219_RAM_TEST, 1 );			//display test mode
	else
		max7219_write( MAX7219_RAM_TEST, 0 );			//normal operation.
 } 
 
 /**
  * Toggle for enabling RAM_SHUTDOWN.
  *
  * param: enable_shutdown - boolean for toggling the RAM_SHUTDOWN mode. 
  *        True for enabling RAM_SHUTDOWN, False for disabling (normal operation). 
  * return: void
  */
  void max7219_shutdown( bool enable_shutdown ) {
	if( enable_shutdown )
		max7219_write( MAX7219_RAM_SHUTDOWN, 0 );		//shutdown mode.
	else
		max7219_write( MAX7219_RAM_SHUTDOWN, 1 );		//normal operation.
  }
  
 /**
  * A debugging utility that displays unassigned 8bit integers (uint8_t) for three seconds
  * on the first column for the 8x8 matrix, after which it clears the display.  
  *
  * param: uint8_t data, - the debug data to be written first column.
  * return: void
  */
  void debug( uint8_t data ) {
    max7219_clear( );				//clear the matrix display.
	max7219_write( 1, data );		//write the debug data to the first column of the display.
	_delay_ms( 3000 );				//wait three seconds.
	
  }
  
 /**
  * A debugging utility that displays unassigned 8bit integers (uint8_t) for three seconds
  * on the first column for the 8x8 matrix, after which it clears the display.  
  *
  * param: uint8_t data, - the debug data to be written first column.
  * param: uint8_t column - the column to be cleared.
  * param: int delayms - the delay (in milliseconds) to pause before continuing.
  * return: void
  */
  void debugc( uint8_t data, uint8_t column, int delayms  ) {
	max7219_clear( column );		//clear the matrix display.
	max7219_write( column, data );	//write the debug data to the first column of the display.
	_delay_ms( delayms );			//wait before continuing.
  }
 
 