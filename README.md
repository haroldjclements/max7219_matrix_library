# README #

This is an Atmel AVR library for controlling an [8x8 LED Matrix](http://forums.parallax.com/uploads/attachments/53043/102009.jpg) driven by [Maxim 7219](https://www.maximintegrated.com/en/products/power/display-power-control/MAX7219.html) driver.

The bitmap font definitions are based of the 8x8 PCB assembly being positioned with the interface pins facing downwards, for connecting directly into a breadboard. This has the effect of each "segment" (character byte) being displayed as column on the actual device.   

The original code was developed by [Andrey Ovcharov](https://github.com/snakeye/avr-projects/tree/master/max7219). This library extends the original interface and contains following definitions.

## Version
1.3.0.0 [20160616] Added string scrolling.

#### Limitations
* This driver library currently only supports one 8x8 LED Matrix.
* Column "segment" (character byte) display.
* Only a subset of the full 255 ASCCI characters set supported. 

#### Future Enhancements
* Support more than one 8x8 Matrix (Max7219).
* Support "Marquee" type text scrolling.
* Support multi rotational fonts / display.

## Interface Definition
___________
```c
/*
 * Initialisation for setting up the MAX7219 device.
 */
void max7219_init( );
```

___________

```c
/*
 * Writes the data to a specific register.
 * The two input parameter bytes are shifted into the internal 16-bit register. 
 * The load pin is turned on before the data sent to the max7219_write function.
 * After both the data address and digit data are sent to the MAX7219, 
 *
 * | D15  D14  D13  D12   D11  D10  D09  D08 |  D07  D06  D05  D04  D03  D02  D01  D00 |
 * |  X    X    X    X  |       ADDRESS      |  MSB              DATA              LSB |
 * 
 */
void max7219_write( uint8_t addr, uint8_t data );
```
| Parameter        | Description|
| :-------------: |-------------| 
| addr    | The address register to which to write the data. |
| data    | A byte string of data to be written to the register.     |

___________

```c
/*
 * Iterates through the byte within the data array, sending each byte in turn to the max7219_write function.   
 */
void max7219_writebytes( const uint8_t* data );
```
| Parameter        | Description|
| :-------------: |-------------| 
| *data    | A pointer to the 'data' byte (character bitmap) array.     |

___________

```c
/*
 * Iterates through a given string each character at a time. A new byte array (pointer) is created with 8 elements.
 * This pointer is then passed to the max7219_charmappings function to populate the data pointer with the 
 * relevant bitmap array. The populated pointer is then passed to the max7219_writebytes function for display. 
 * The process then pauses for a given amount of time (delayms) before deleting the data array bitmap and 
 * continuing on with the next letter in the given string ( string ).
 */
void max7219_writestring( const char* string, int delayms );
```
| Parameter        | Description|
| :-------------: |-------------| 
| *string    | A pointer to the 'data' byte (character bitmap) array.     |
| delayms    | The time (in milliseconds) to delay before processing the next character.     |

___________

```c
/*
 * Iterates through a given string each character at a time. A new byte array (pointer) is created with 8 elements.
 * This pointer is then passed to the max7219_charmappings function to populate the data pointer with the 
 * relevant bitmap array. A temporary byte array is created and reverse bit-shifted with the populated data array.
 * This scrolls the character in from the left side of the display, moving right. The populated data array is 
 * bit-shifted one to the right to simulate the character scrolling out from the right.
 */
void max7219_scrollstring( const char* string, int delayms );
```
| Parameter        | Description|
| :-------------: |-------------| 
| *string    | A pointer to the string array for the message to be displayed.     |
| delayms    | The scroll speed (in milliseconds) and the before the next character.     |

___________

```c
/*
 * The lookup table (switch) matches the ASCII decimal value (character) and copies the relevant bitmap array into 
 * the (data) given pointer. memcpy_P is used as the font collection (bitmap byte arrays) are stored in the 
 * microcontroller's flash (program space).
 */
 void max7219_charmappings( const char character, uint8_t* data );
```
| Parameter        | Description|
| :-------------: |-------------| 
| const char   | The lookup character for the relevant bitmap array.     |
|  *data   | A pointer to the 8 bite 'data' array.     |

___________

```c
/*
 * Will iterate over every bit within the 'data' byte, turning off the CLK pin and testing to 
 * see if the most significant bit ( & 0x80 ) is set. If true, the DIN pin is set to on. 
 * If false, the DIN pin is set to off. The data byte is then bit shift one to the left ( << 1 ), the CLK pin 
 * turned back on and the iteration is repeated.
 */
void max7219_send( uint8_t data );
```
| Parameter        | Description|
| :-------------: |-------------| 
| data    | The 'data' byte (character bitmap) array.     |

___________

```c
/*
 * Clears (extinguishes) all the LEDs in a given (row).
 */
void max7219_clearrow( uint8_t row );
```
| Parameter        | Description|
| :-------------: |-------------| 
| row    | the row to be cleared.     |

___________
```c
/*
 * Clears (extinguishes) all the LEDs.
 */
void max7219_clear(  );
```

___________
```c
/*
 * Toggles the RAM_TEST mode.
 */
void max7219_test( bool is_enable );
```
| Parameter        | Description|
| :-------------: |-------------| 
| is_enable   | boolean for toggling the RAM_TEST mode.True for enabling RAM_TEST, False for disabling.     |

___________
```c
/*
 * Toggle for enabling RAM_SHUTDOWN.
 */
void max7219_shutdown( bool enable_shutdown );
```
| Parameter        | Description|
| :-------------: |-------------| 
| enable_shutdown   |  boolean for toggling the RAM_SHUTDOWN mode. True for enabling RAM_SHUTDOWN, False for disabling (normal operation).      |
 
 _________
```c
/*
 * A debugging utility that displays unassigned 8bit integers (uint8_t) for three seconds on the first column 
 * for the 8x8 matrix, after which it clears the display.
 */
void debug( uint8_t data );
```
| Parameter        | Description|
| :-------------: |-------------| 
| data   | The debug data to be written to the first column.   |

 _________
```c
/*
 * A debugging utility that displays an unsigned 8bit integers (uint8_t) to a given (column) 
 * for a given amount of time (delayms), after which it clears the display. 
 */
void debugc( uint8_t data, uint8_t column, int delayms  );
```
| Parameter        | Description|
| :-------------: |-------------| 
| data   | The debug data to be written.   |
| column | The column to which to display the data. |
| delayms | The delay before clearing the display. |